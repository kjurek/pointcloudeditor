#ifndef COHERENTCOMPONENTSWIDGET_HH
#define COHERENTCOMPONENTSWIDGET_HH

#include <QDialog>
#include <QListWidgetItem>
#include <set>
#include "Common.hh"
#include "CoherentComponents.hh"

namespace Ui {
class CoherentComponentsWidget;
}

class CoherentComponentsWidget : public QDialog
{
    Q_OBJECT

public:
    explicit CoherentComponentsWidget(QWidget *parent = 0);
    ~CoherentComponentsWidget();

signals:
    void getPoints(std::vector< Point<double> >& );
    void colourPoints(std::vector< Point<double> >&, QColor);
    void uncolourPoints(std::vector< Point<double> >&);
    void deleteSelected(std::vector< Point<double> >&);

protected slots:
    void handleStartButton();
    void handleDeleteSelectedButton();
    void handleComponentSelected(int id);
    void handleComponentUnselected(int id);
    void handleItemChanged (QListWidgetItem* item);

protected:
    virtual void reject();
private:
    Ui::CoherentComponentsWidget *ui;
    std::vector<CoherentComponents< Point<double> >::Component> m_components;
    void clearWidget();
};

#endif // COHERENTCOMPONENTSWIDGET_HH
