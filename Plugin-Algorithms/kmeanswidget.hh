#ifndef KMEANSWIDGET_HH
#define KMEANSWIDGET_HH

#include <QDialog>
#include <vector>

namespace Ui {
class KMeansWidget;
}

class KMeansWidget : public QDialog
{
    Q_OBJECT

public:
    explicit KMeansWidget(QWidget *parent = 0);
    ~KMeansWidget();

    std::vector<QColor>& colors();
    unsigned int clusters() const;
    unsigned int iterations() const;

protected:
    void accept();
private slots:
    void handleSetColors();
private:
    Ui::KMeansWidget *ui;
    std::vector<QColor> m_colors;
    unsigned int m_clusters;
    unsigned int m_iterations;
};

#endif // KMEANSWIDGET_HH
