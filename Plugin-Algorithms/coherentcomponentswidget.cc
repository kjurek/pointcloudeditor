#include <QDebug>
#include <vector>
#include <QCheckBox>
#include <list>
#include "coherentcomponentswidget.hh"
#include "ui_coherentcomponentswidget.hh"

CoherentComponentsWidget::CoherentComponentsWidget(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CoherentComponentsWidget)
{
    ui->setupUi(this);
    connect(ui->startButton, SIGNAL(released()), this, SLOT(handleStartButton()));
    connect(ui->deleteSelectedButton, SIGNAL(released()), this, SLOT(handleDeleteSelectedButton()));
    connect(ui->listWidget, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(handleItemChanged(QListWidgetItem*)));
}

CoherentComponentsWidget::~CoherentComponentsWidget()
{
    delete ui;
}

void CoherentComponentsWidget::handleStartButton()
{
    std::vector< Point<double> > points;
    emit getPoints(points);
    if(points.size() == 0)
    {
        return;
    }
    CoherentComponents< Point<double> > alg(points);
    alg.start();
    m_components = alg.components();

    for(int i = 0; i < m_components.size(); ++i)
    {
        QListWidgetItem *listItem = new QListWidgetItem(tr("Component ") + QString::number(i) + " (" + QString::number(m_components[i].points.size()) + " points)", ui->listWidget);
        listItem->setCheckState(Qt::Unchecked);
        listItem->setData(Qt::UserRole, QVariant(i));
        ui->listWidget->addItem(listItem);
    }
    ui->deleteSelectedButton->setEnabled(true);
}

void CoherentComponentsWidget::handleDeleteSelectedButton()
{
    std::vector< Point<double> > points;
    std::list< int > selected;
    int nPoints = 0;

    for(int i = 0; i < ui->listWidget->count(); ++i)
    {
        QListWidgetItem* item = ui->listWidget->item(i);
        if(item->checkState() == Qt::Checked)
        {
            int componentIndex = item->data(Qt::UserRole).toInt();
            selected.push_back(componentIndex);
            nPoints += m_components[i].points.size();
        }
    }
    points.reserve(nPoints);

    for(auto idx : selected)
    {
        points.insert(points.end(), m_components[idx].points.begin(), m_components[idx].points.end());
    }

    emit deleteSelected(points);
    m_components.clear();
    ui->listWidget->clear();
    ui->deleteSelectedButton->setEnabled(false);
    QDialog::accept();
}
void CoherentComponentsWidget::handleComponentSelected(int id)
{
    QColor color(0, 0, 255);
    emit colourPoints(m_components[id].points, color);
}

void CoherentComponentsWidget::handleComponentUnselected(int id)
{
    emit uncolourPoints(m_components[id].points);
}

void CoherentComponentsWidget::handleItemChanged(QListWidgetItem *item)
{
    if(!ui->deleteSelectedButton->isEnabled())
    {
        return;
    }
    QVariant data = item->data(Qt::UserRole);

    if(item->checkState() == Qt::Checked)
    {
        handleComponentSelected(data.toInt());
    }
    else
    {
        handleComponentUnselected(data.toInt());
    }
}

void CoherentComponentsWidget::reject()
{
    clearWidget();
    QDialog::reject();
}

void CoherentComponentsWidget::clearWidget()
{
    for(int i = 0; i < ui->listWidget->count(); ++i)
    {
        QListWidgetItem* item = ui->listWidget->item(i);
        emit uncolourPoints(m_components[item->data(Qt::UserRole).toInt()].points);
    }
    m_components.clear();
    ui->listWidget->clear();
    ui->deleteSelectedButton->setEnabled(false);
}
