#include <QDebug>
#include <QPalette>
#include <QColorDialog>
#include <random>
#include "clustercolorchoosewidget.hh"
#include "ui_clustercolorchoosewidget.hh"

ClusterColorChooseWidget::ClusterColorChooseWidget(int num, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ClusterColorChooseWidget),
    m_clusterNum(num)
{
    ui->setupUi(this);
    ui->clusterNameLabel->setText(tr("Cluster ") + QString::number(num));
    QPalette palette = ui->colorLabel->palette();
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, 255);
    m_color = QColor(dis(gen), dis(gen), dis(gen));
    palette.setColor(QPalette::Window, m_color);
    ui->colorLabel->setPalette(palette);
    connect(ui->colorLabel, SIGNAL(clicked()), this, SLOT(handleColorChoose()));
}

ClusterColorChooseWidget::~ClusterColorChooseWidget()
{
    delete ui;
}

void ClusterColorChooseWidget::handleColorChoose()
{
    m_color = QColorDialog::getColor(m_color, this);
    QPalette palette = ui->colorLabel->palette();
    palette.setColor(QPalette::Window, m_color);
    ui->colorLabel->setPalette(palette);
}

QColor ClusterColorChooseWidget::color() const
{
    return m_color;
}

int ClusterColorChooseWidget::clusterNum() const
{
    return m_clusterNum;
}

