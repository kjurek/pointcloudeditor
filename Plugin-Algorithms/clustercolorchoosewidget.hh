#ifndef CLUSTERCOLORCHOOSEWIDGET_HH
#define CLUSTERCOLORCHOOSEWIDGET_HH

#include <QWidget>

namespace Ui {
class ClusterColorChooseWidget;
}

class ClusterColorChooseWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ClusterColorChooseWidget(int num, QWidget *parent = 0);
    ~ClusterColorChooseWidget();
    int clusterNum() const;
    QColor color() const;

private slots:
    void handleColorChoose();
private:
    Ui::ClusterColorChooseWidget *ui;
    int m_clusterNum;
    QColor m_color;
};

#endif // CLUSTERCOLORCHOOSEWIDGET_HH
