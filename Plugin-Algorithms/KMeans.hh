#ifndef KMEANS_HH
#define KMEANS_HH

#include <vector>
#include <random>
#include <limits>
#include <QDebug>
#include "Common.hh"

template <typename PointType>
class KMeans
{
public:
    KMeans(std::vector<PointType>& points, unsigned int nClusters, unsigned int nIterations) :
        m_points(points), m_nClusters(nClusters), m_nIterations(nIterations),
        m_clustering(points.size(), 0), m_clusterPointsNumber(nClusters, 0)
    {
        m_centers.reserve(m_nClusters);
    }

    void start()
    {
        std::random_device rd;
        std::mt19937 mt(rd());
        std::uniform_int_distribution<> dist(0, m_points.size() - 1);
        PointType center;

        for(unsigned int i = 0; i < m_nClusters; ++i)
        {
            m_centers.push_back(m_points[dist(mt)]);
        }

        for(unsigned int i = 0; i < m_nIterations; ++i)
        {
            assignPointsToClusters();
            assignNewCenters();
        }
    }
    std::vector<PointType>& centers() { return m_centers; }
    std::vector<unsigned int>& clustering() { return m_clustering; }
    std::vector<unsigned int>& clusterPointsNumber() { return m_clusterPointsNumber; }

private:
    void assignPointsToClusters()
    {
        for(unsigned i = 0; i < m_clusterPointsNumber.size(); ++i)
        {
            m_clusterPointsNumber[i] = 0;
        }

        for(unsigned int i = 0; i < m_points.size(); ++i)
        {
            double min_dist = std::numeric_limits<double>::max();
            double curr_dist = 0;
            unsigned int best_cluster = 0;
            for(unsigned int k = 0; k < m_centers.size(); ++k)
            {
                curr_dist = distance(m_centers[k], m_points[i]);
                if(curr_dist < min_dist)
                {
                    min_dist = curr_dist;
                    best_cluster = k;
                }
            }
            m_clustering[i] = best_cluster;
            m_clusterPointsNumber[best_cluster]++;
        }
    }

    void assignNewCenters()
    {
        for(unsigned int i = 0; i < m_centers.size(); ++i)
        {
            m_centers[i].x = 0;
            m_centers[i].y = 0;
            m_centers[i].z = 0;
        }

        for(unsigned int i = 0; i < m_points.size(); ++i)
        {
            PointType& p = m_centers[m_clustering[i]];
            double pointsInClusters = static_cast<double>(m_clusterPointsNumber[m_clustering[i]]);
            p.x += m_points[i].x / pointsInClusters;
            p.y += m_points[i].y / pointsInClusters;
            p.z += m_points[i].z / pointsInClusters;
        }
    }

private:
    std::vector<PointType>& m_points;
    unsigned int m_nClusters;
    unsigned int m_nIterations;
    std::vector<PointType> m_centers;
    std::vector<unsigned int> m_clustering;
    std::vector<unsigned int> m_clusterPointsNumber;
};

#endif


