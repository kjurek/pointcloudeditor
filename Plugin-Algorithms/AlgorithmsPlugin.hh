#ifndef ALGORITHMSPLUGIN
#define ALGORITHMSPLUGIN

#include <QObject>
#include <QMenuBar>

#include <OpenFlipper/common/Types.hh>
#include <OpenFlipper/BasePlugin/BaseInterface.hh>
#include <OpenFlipper/BasePlugin/MenuInterface.hh>
#include <OpenFlipper/BasePlugin/LoggingInterface.hh>
#include <ObjectTypes/PolyMesh/PolyMesh.hh>

#include "Common.hh"
#include "kmeanswidget.hh"
#include "coherentcomponentswidget.hh"

class AlgorithmsPlugin : public QObject, BaseInterface, MenuInterface, LoggingInterface
{
    Q_OBJECT
    Q_INTERFACES(LoggingInterface)
    Q_INTERFACES(BaseInterface)
    Q_INTERFACES(MenuInterface)

#if QT_VERSION >= 0x050000
    Q_PLUGIN_METADATA(IID "org.OpenFlipper.Plugins.Plugin-Algorithms")
#endif

signals:
    void addMenubarAction(QAction* _action, QString _type );
    void getMenubarMenu (QString _name, QMenu *&_menu, bool _create);

    //LoggingInterface
    void log(Logtype _type, QString _message);
    void log(QString _message);

private slots:
    void pluginsInitialized();

public :
    QString name() { return (QString("ALGORITHMS Plugin")); }
    QString description( ) { return (QString(tr("Provides basic algorithms"))); }

public slots:
    void getPoints(std::vector< Point<double> >& points);
    void kmeans();
    void handleCoherentComponentsDeleteSelected(std::vector< Point<double> >& points);
    void handleColourPoints(std::vector< Point<double> >& points, QColor color);
    void handleUnColourPoints(std::vector< Point<double> >& points);

private:
    std::vector< PolyMesh::Color > convertColors(std::vector<QColor>& colors);
    KMeansWidget m_kmeansWidget;
    CoherentComponentsWidget m_coherentComponentsWidget;
public slots:
    QString version() { return QString("1.0"); }
};

#endif
