#include <QPrintDialog>
#include <QPrinter>

#include "AlgorithmsPlugin.hh"
#include "kmeanswidget.hh"
#include "coherentcomponentswidget.hh"
#include "KMeans.hh"

#include <iostream>
#include <sstream>
#include <ACG/GL/GLState.hh>

#include <OpenFlipper/BasePlugin/PluginFunctions.hh>
#include <OpenFlipper/common/GlobalOptions.hh>
#include <ObjectTypes/TriangleMesh/TriangleMesh.hh>


#if QT_VERSION >= 0x050000
#include <QtWidgets>
#else
#include <QtGui>
#endif

void AlgorithmsPlugin::pluginsInitialized()
{
    QMenu* menu = 0;
    emit getMenubarMenu(ALGORITHMMENU, menu, true);

    QAction* AC_kmeans = new QAction(tr("&K-Means"), menu);
    QAction* AC_coherentComponents = new QAction(tr("&Coherent components"), this);

    connect(AC_kmeans, SIGNAL(triggered()), &m_kmeansWidget, SLOT(exec()));
    connect(&m_kmeansWidget, SIGNAL(accepted()), this, SLOT(kmeans()));
    connect(AC_coherentComponents, SIGNAL(triggered()), &m_coherentComponentsWidget, SLOT(show()));
    connect(&m_coherentComponentsWidget, SIGNAL(deleteSelected(std::vector<Point<double> >&)), this, SLOT(handleCoherentComponentsDeleteSelected(std::vector<Point<double> >&)));
    connect(&m_coherentComponentsWidget, SIGNAL(getPoints(std::vector<Point<double> >&)), this, SLOT(getPoints(std::vector<Point<double> >&)));

    emit addMenubarAction(AC_kmeans, ALGORITHMMENU);
    emit addMenubarAction(AC_coherentComponents, ALGORITHMMENU);
    connect(&m_coherentComponentsWidget, SIGNAL(colourPoints(std::vector<Point<double> >&, QColor)), this, SLOT(handleColourPoints(std::vector<Point<double> >&, QColor)));
    connect(&m_coherentComponentsWidget, SIGNAL(uncolourPoints(std::vector<Point<double> >&)), this, SLOT(handleUnColourPoints(std::vector<Point<double> >&)));
}

void AlgorithmsPlugin::kmeans()
{
    std::vector< Point<double> > points;
    getPoints(points);

    if(points.size() == 0)
    {
        return;
    }

    KMeans< Point<double> > alg(points, m_kmeansWidget.clusters(), m_kmeansWidget.iterations());
    alg.start();
    auto& clustering = alg.clustering();
    auto& centers = alg.centers();
    auto colors = convertColors(m_kmeansWidget.colors());
    int idx = 0;

    for (PluginFunctions::ObjectIterator o_it(PluginFunctions::TARGET_OBJECTS); o_it != PluginFunctions::objectsEnd();
         ++o_it)
    {
        PolyMesh* mesh = PluginFunctions::polyMesh(*o_it);
        PolyMesh::VertexIter v_it, v_end = mesh->vertices_end();
        for (v_it = mesh->vertices_begin(); v_it != v_end; ++v_it)
        {
            mesh->set_color(*v_it, colors[clustering[idx]]);
            ++idx;
        }
        o_it->update();
    }
    qDebug() << "done";
}

void AlgorithmsPlugin::handleCoherentComponentsDeleteSelected(std::vector<Point<double> > &points)
{
    for(auto& point : points)
    {
        for (PluginFunctions::ObjectIterator o_it(PluginFunctions::TARGET_OBJECTS); o_it != PluginFunctions::objectsEnd();
             ++o_it)
        {
            if(o_it->id() != point.data.meshId)
            {
                continue;
            }

            if (o_it->dataType(DATA_TRIANGLE_MESH))
            {
                TriMesh* mesh = PluginFunctions::triMesh(*o_it);
                TriMesh::VertexHandle vHandle = mesh->vertex_handle(point.data.index);
                mesh->delete_vertex(vHandle, false);
            }
            else if (o_it->dataType(DATA_POLY_MESH))
            {
                PolyMesh* mesh = PluginFunctions::polyMesh(*o_it);
                PolyMesh::VertexHandle vHandle = mesh->vertex_handle(point.data.index);
                mesh->delete_vertex(vHandle, false);
            }
            else
            {
                emit log(LOGERR, "Data type not supported.");
            }
        }
    }

    for (PluginFunctions::ObjectIterator o_it(PluginFunctions::TARGET_OBJECTS); o_it != PluginFunctions::objectsEnd();
         ++o_it)
    {

        if (o_it->dataType(DATA_TRIANGLE_MESH))
        {
            TriMesh* mesh = PluginFunctions::triMesh(*o_it);
            mesh->garbage_collection();
        }
        else if (o_it->dataType(DATA_POLY_MESH))
        {
            PolyMesh* mesh = PluginFunctions::polyMesh(*o_it);
            mesh->garbage_collection();
        }
        else
        {
            emit log(LOGERR, "Data type not supported.");
        }
        o_it->update();
    }
}

void AlgorithmsPlugin::handleColourPoints(std::vector<Point<double> > &points, QColor color)
{
    for(auto& point : points)
    {
        for (PluginFunctions::ObjectIterator o_it(PluginFunctions::TARGET_OBJECTS); o_it != PluginFunctions::objectsEnd();
             ++o_it)
        {
            if(o_it->id() != point.data.meshId)
            {
                continue;
            }

            if (o_it->dataType(DATA_TRIANGLE_MESH))
            {
                TriMesh* mesh = PluginFunctions::triMesh(*o_it);
                TriMesh::VertexHandle vHandle = mesh->vertex_handle(point.data.index);
                mesh->set_color(vHandle, TriMesh::Color(color.redF(), color.greenF(), color.blueF(), color.alphaF()));
            }
            else if (o_it->dataType(DATA_POLY_MESH))
            {
                PolyMesh* mesh = PluginFunctions::polyMesh(*o_it);
                PolyMesh::VertexHandle vHandle = mesh->vertex_handle(point.data.index);
                mesh->set_color(vHandle, PolyMesh::Color(color.redF(), color.greenF(), color.blueF(), color.alphaF()));
            }
            else
            {
                emit log(LOGERR, "Data type not supported.");
            }
        }
    }

    for (PluginFunctions::ObjectIterator o_it(PluginFunctions::TARGET_OBJECTS); o_it != PluginFunctions::objectsEnd();
         ++o_it)
    {
        o_it->update();
    }
}

void AlgorithmsPlugin::handleUnColourPoints(std::vector<Point<double> > &points)
{
    for(auto& point : points)
    {
        for (PluginFunctions::ObjectIterator o_it(PluginFunctions::TARGET_OBJECTS); o_it != PluginFunctions::objectsEnd();
             ++o_it)
        {
            if(o_it->id() != point.data.meshId)
            {
                continue;
            }

            if (o_it->dataType(DATA_TRIANGLE_MESH))
            {
                TriMesh* mesh = PluginFunctions::triMesh(*o_it);
                TriMesh::VertexHandle vHandle = mesh->vertex_handle(point.data.index);
                mesh->set_color(vHandle, TriMesh::Color(point.data.r, point.data.g, point.data.b, point.data.a));
            }
            else if (o_it->dataType(DATA_POLY_MESH))
            {
                PolyMesh* mesh = PluginFunctions::polyMesh(*o_it);
                PolyMesh::VertexHandle vHandle = mesh->vertex_handle(point.data.index);
                mesh->set_color(vHandle, PolyMesh::Color(point.data.r, point.data.g, point.data.b, point.data.a));
            }
            else
            {
                emit log(LOGERR, "Data type not supported.");
            }
        }
    }

    for (PluginFunctions::ObjectIterator o_it(PluginFunctions::TARGET_OBJECTS); o_it != PluginFunctions::objectsEnd();
         ++o_it)
    {
        o_it->update();
    }
}

void AlgorithmsPlugin::getPoints(std::vector<Point<double> > &points)
{
    for (PluginFunctions::ObjectIterator o_it(PluginFunctions::TARGET_OBJECTS); o_it != PluginFunctions::objectsEnd();
         ++o_it)
    {
        int id = 0;

        if (o_it->dataType(DATA_TRIANGLE_MESH))
        {
            TriMesh* mesh = PluginFunctions::triMesh(*o_it);
            TriMesh::VertexIter v_it, v_end = mesh->vertices_end();
            Point<double> p2;
            for (v_it = mesh->vertices_begin(); v_it != v_end; ++v_it)
            {
                TriMesh::Point p = mesh->point(*v_it);
                p2.x = p[0];
                p2.y = p[1];
                p2.z = p[2];
                p2.data.index = id++;
                p2.data.meshId = o_it->id();
                auto c = mesh->color(*v_it);
                p2.data.r = c[0];
                p2.data.g = c[1];
                p2.data.b = c[2];
                p2.data.a = c[3];
                points.push_back(p2);
            }
        }
        else if (o_it->dataType(DATA_POLY_MESH))
        {
            PolyMesh* mesh = PluginFunctions::polyMesh(*o_it);
            PolyMesh::VertexIter v_it, v_end = mesh->vertices_end();
            Point<double> p2;
            for (v_it = mesh->vertices_begin(); v_it != v_end; ++v_it)
            {
                PolyMesh::Point p = mesh->point(*v_it);
                p2.x = p[0];
                p2.y = p[1];
                p2.z = p[2];
                p2.data.index = id++;
                p2.data.meshId = o_it->id();
                auto c = mesh->color(*v_it);
                p2.data.r = c[0];
                p2.data.g = c[1];
                p2.data.b = c[2];
                p2.data.a = c[3];
                points.push_back(p2);
            }
        }
        else
        {
            emit log(LOGERR, "Data type not supported.");
        }
    }
}

std::vector<PolyMesh::Color> AlgorithmsPlugin::convertColors(std::vector<QColor> &colors)
{
    std::vector<PolyMesh::Color> result;
    result.reserve(colors.size());

    for(auto& color : colors)
    {
        result.push_back(PolyMesh::Color(color.redF(), color.greenF(), color.blueF(), color.alphaF()));
    }
    return result;
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2( algorithmsplugin , AlgorithmsPlugin );
#endif

