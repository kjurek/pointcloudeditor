#ifndef COHERENTCOMPONENTS_H
#define COHERENTCOMPONENTS_H

#include <map>
#include <stack>
#include <iostream>
#include <vector>
#include <bits/stl_set.h>

template <typename PointType>
class CoherentComponents
{
public:
    struct Component {
        std::vector<PointType> points;
        PointType min, max;
    };
public:
    CoherentComponents(std::vector<PointType>& points)
        :   nTotalPoints(points.size()), nVisited(0), pointsSet(points.begin(), points.end())
    { }

    void start()
    {
        while(nVisited != nTotalPoints)
        {
            calculateComponent();
        }
    }

    std::vector<Component>& components() { return m_components; }

private:
    void calculateComponent()
    {
        std::stack<PointType> pointsToVisit;
        Component c;

        PointType first = *pointsSet.begin();
        pointsToVisit.push(first);
        pointsSet.erase(pointsSet.begin());

        while(!pointsToVisit.empty())
        {
            PointType currPoint = pointsToVisit.top();
            pointsToVisit.pop();
            ++nVisited;
            c.points.push_back(currPoint);

            auto it = pointsSet.find({currPoint.x + 1, currPoint.y, currPoint.z});
            if(it != pointsSet.end())
            {
                pointsToVisit.push(*it);
                pointsSet.erase(it);
            }
            it = pointsSet.find({currPoint.x - 1, currPoint.y, currPoint.z});
            if(it != pointsSet.end())
            {
                pointsToVisit.push(*it);
                pointsSet.erase(it);
            }
            it = pointsSet.find({currPoint.x, currPoint.y + 1, currPoint.z});
            if(it != pointsSet.end())
            {
                pointsToVisit.push(*it);
                pointsSet.erase(it);
            }
            it = pointsSet.find({currPoint.x, currPoint.y - 1, currPoint.z});
            if(it != pointsSet.end())
            {
                pointsToVisit.push(*it);
                pointsSet.erase(it);
            }
            it = pointsSet.find({currPoint.x, currPoint.y, currPoint.z + 1});
            if(it != pointsSet.end())
            {
                pointsToVisit.push(*it);
                pointsSet.erase(it);
            }
            it = pointsSet.find({currPoint.x, currPoint.y, currPoint.z - 1});
            if(it != pointsSet.end())
            {
                pointsToVisit.push(*it);
                pointsSet.erase(it);
            }
        }
        m_components.push_back(c);
    }

private:
    std::set<PointType> pointsSet;
    std::vector<Component> m_components;
    size_t nTotalPoints;
    size_t nVisited;
};

#endif // COHERENTCOMPONENTS_H
