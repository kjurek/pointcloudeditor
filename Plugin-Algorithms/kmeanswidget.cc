#include <QDebug>
#include <QVBoxLayout>
#include "kmeanswidget.hh"
#include "ui_kmeanswidget.hh"
#include "clustercolorchoosewidget.hh"

KMeansWidget::KMeansWidget(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::KMeansWidget)
{
    ui->setupUi(this);
    connect(ui->setColorsButton, SIGNAL(released()), this, SLOT(handleSetColors()));
}

KMeansWidget::~KMeansWidget()
{
    delete ui;
}

void KMeansWidget::accept()
{
    if(ui->verticalLayout->isEmpty())
    {
        handleSetColors();
    }

    for(int i = 0; i < ui->verticalLayout->count(); ++i)
    {
        QLayoutItem* item = ui->verticalLayout->itemAt(i);
        if(dynamic_cast<QWidgetItem *>(item))
        {
            ClusterColorChooseWidget* widget = dynamic_cast<ClusterColorChooseWidget*>(item->widget());
            if(widget)
            {
                m_colors.push_back(widget->color());
            }
        }
    }

    m_clusters = ui->clustersSB->value();
    m_iterations = ui->iterationsSB->value();

    QDialog::accept();
}

void KMeansWidget::handleSetColors()
{
    if(!ui->verticalLayout->isEmpty())
    {
        QLayoutItem* child;
        while ((child = ui->verticalLayout->takeAt(0)) != 0)
        {
            delete child;
        }
    }

    int nClusters = ui->clustersSB->value();

    for(int i = 0; i < nClusters; ++i)
    {
        ui->verticalLayout->addWidget(new ClusterColorChooseWidget(i));
    }
}
unsigned int KMeansWidget::iterations() const
{
    return m_iterations;
}

unsigned int KMeansWidget::clusters() const
{
    return m_clusters;
}

std::vector<QColor>& KMeansWidget::colors()
{
    return m_colors;
}

