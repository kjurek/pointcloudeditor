#include "clickablelabel.hh"

ClickableLabel::ClickableLabel(QWidget * parent) :
    QLabel(parent)

{
}

ClickableLabel::ClickableLabel(const QString &text, QWidget *parent, Qt::WindowFlags f) :
    QLabel(text, parent, f)
{
}

ClickableLabel::~ClickableLabel()
{
}

void ClickableLabel::mousePressEvent ( QMouseEvent * event )

{
    emit clicked();
}
