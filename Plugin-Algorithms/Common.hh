#ifndef COMMON_H_
#define COMMON_H_

#include <cmath>

struct BasicPointData
{
    int index;
    int meshId;
    float r, g, b, a;
};

template <typename T, typename AditionalData = BasicPointData>
struct Point {
    T x, y, z;
    AditionalData data;
    friend bool operator<(const Point<T>& p1, const Point<T>& p2) {
        if(p1.z < p2.z)
        {
            return true;
        } else if(p1.z == p2.z) {
            if(p1.y < p2.y) {
                return true;
            } else if(p1.y == p2.y) {
                if(p1.x < p2.x) {
                    return true;
                }
            }
        }
        return false;
    }

    friend bool operator==(const Point<T>& p1, const Point<T>& p2) {
        return (p1.x == p2.x) && (p1.y == p2.y) && (p1.z == p2.z);
    }

    friend bool operator!=(const Point<T>& p1, const Point<T>& p2) {
        return !(p1 == p2);
    }
};

template <typename PointType>
double distance(const PointType& p1, const PointType& p2)
{
    double a = p1.x - p2.x;
    double b = p1.y - p2.y;
    double c = p1.z - p2.z;

    return std::sqrt(a*a + b*b + c*c);
}

#endif
