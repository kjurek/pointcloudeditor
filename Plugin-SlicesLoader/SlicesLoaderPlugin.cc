#include <fstream>
#include <QFileDialog>
#include "SlicesLoaderPlugin.hh"
#include "slicesloaderdialog.hh"

void SlicesLoaderPlugin::pluginsInitialized()
{
    QAction* AC_LoadSlices = new QAction(tr("&Export slices to PLY"), this);
    emit addMenubarAction(AC_LoadSlices, FILEMENU );
    connect(AC_LoadSlices, SIGNAL(triggered()), this, SLOT(loadSlices()));
}

void SlicesLoaderPlugin::loadSlices()
{
    SlicesLoaderDialog d(0);
    d.exec();
    QString exportPath = QFileDialog::getSaveFileName(0, tr("Export PLY"), QDir::homePath(), tr("PLY files (*.ply)"));
    std::vector< cv::Mat > originalImages = d.originalImages();
    std::vector< cv::Mat > filteredImages = d.filteredImages();
    assert(originalImages.size() == filteredImages.size());
    int count = 0;

    for(int i = 0; i < filteredImages.size(); ++i)
    {
        for(int row = 0; row < filteredImages[i].rows; ++row)
        {
            uchar* pFiltered = filteredImages[i].ptr(row);
            for(int col = 0; col < filteredImages[i].cols; ++col)
            {
                if(pFiltered[col] != 0)
                {
                    ++count;
                }
            }
        }
    }
    std::ofstream output(exportPath.toStdString());
    output << "ply" << std::endl;
    output << "format ascii 1.0" << std::endl;
    output << "element vertex " << count << std::endl;
    output << "property int32 x" << std::endl;
    output << "property int32 y" << std::endl;
    output << "property int32 z" << std::endl;
    output << "property uchar red" << std::endl;
    output << "property uchar green" << std::endl;
    output << "property uchar blue" << std::endl;
    output << "end_header" << std::endl;

    for(int i = 0; i < filteredImages.size(); ++i)
    {
        for(int row = 0; row < filteredImages[i].rows; ++row)
        {
            uchar* pFiltered = filteredImages[i].ptr(row);
            uchar* pOriginal = originalImages[i].ptr(row);
            for(int col = 0; col < filteredImages[i].cols; ++col)
            {
                if(pFiltered[col] != 0)
                {
                    int b = pOriginal[3*col];
                    int g = pOriginal[3*col + 1];
                    int r = pOriginal[3*col + 2];

                    output << col << " " << row << " " << i << " " << r << " " << g << " " << b << std::endl;
                }
            }
        }
    }
    output.close();
}

#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2( slicesloaderplugin , SlicesLoaderPlugin );
#endif
