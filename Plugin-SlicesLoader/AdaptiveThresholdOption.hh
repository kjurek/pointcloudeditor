#ifndef __AdaptiveTresholdOption_H_
#define __AdaptiveTresholdOption_H_

#include "Option.hh"

enum AdaptiveThresholdType
{
    Mean, Gaussian
};

class AdaptiveThresholdOption : public Option {
public:
    AdaptiveThresholdOption() : m_thresholdType(Mean), m_blockSize(1), m_constant(1) { }
    AdaptiveThresholdOption(std::istream& input) { parseInput(input); };

    virtual void parseInput(std::istream& input);
    static std::string avaiableOptions();


    AdaptiveThresholdType thresholdType() const;
    void setThresholdType(const AdaptiveThresholdType &thresholdType);

    unsigned int blockSize() const;
    void setBlockSize(unsigned int blockSize);

    unsigned int constant() const;
    void setConstant(unsigned int constant);

private:
    AdaptiveThresholdType m_thresholdType;
    unsigned int m_blockSize;
    unsigned int m_constant;
};


#endif //__AdaptiveTresholdOption_H_
