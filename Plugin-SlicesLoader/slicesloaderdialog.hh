#ifndef SLICESLOADERDIALOG_HH
#define SLICESLOADERDIALOG_HH

#include <QDialog>
#include <QFileSystemModel>
#include <QListWidgetItem>
#include <vector>
#include "operationwidget.hh"

namespace Ui {
class SlicesLoaderDialog;
}

class SlicesLoaderDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SlicesLoaderDialog(QWidget *parent = 0);
    ~SlicesLoaderDialog();

    std::vector<cv::Mat> filteredImages() const;
    std::vector<cv::Mat> originalImages() const;

private slots:
    void handleBrowse();
    void handleAddFilter();
    void handleFileDoubleClick(QModelIndex index);
    void handleOperationDoubleClick(QModelIndex index);
    void handleDeleteFilter();
    void accept();
private:
    Ui::SlicesLoaderDialog *ui;
    QFileSystemModel* model;
    std::vector< std::shared_ptr<ImageCommand> > m_operations;
    std::vector< cv::Mat > m_filteredImages;
    std::vector< cv::Mat > m_originalImages;
    QStringList m_filters;

    bool isImage(const QFileInfo& info);
    QFileInfoList getSelectedFiles();
    cv::Mat processImage(QFileInfo file);
};

#endif // SLICESLOADERDIALOG_HH
