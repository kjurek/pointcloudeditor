#ifndef __ContourOption_H_
#define __ContourOption_H_

#include "Option.hh"

enum MorphologyOperationType {
    Opening, Closing, Gradient, TopHat, BlackHat, Dilatation, Erosion
};

enum MorphologyElementType {
    Rect, Cross, Ellipse
};

class MorphologyOption : public Option {
public:
    MorphologyOption() : m_operationType(Opening), m_element(Rect), m_elementSize(1) { }
    MorphologyOption(std::istream& input) { parseInput(input); }

    virtual void parseInput(std::istream& input);
    static std::string avaiableOptions();

    void setOperationType(MorphologyOperationType operation) {
        m_operationType = operation;
    }

    void setElement(MorphologyElementType element) {
        m_element = element;
    }

    void setElementSize(unsigned int size) {
        m_elementSize = size;
    }

    unsigned int getOperationType() const {
        return m_operationType;
    }

    unsigned int getElementSize() const {
        return m_elementSize;
    }

    unsigned int getElement() const {
        return m_element;
    }

private:
    MorphologyOperationType m_operationType;
    MorphologyElementType m_element;
    unsigned int m_elementSize;
};


#endif //__ContourOption_H_
