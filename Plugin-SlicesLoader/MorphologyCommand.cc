#include "MorphologyCommand.hh"

using namespace cv;

Mat MorphologyCommand::execute(Mat &inputImage) {
    unsigned int element = m_options.getElement();
    unsigned int elementSize = m_options.getElementSize();
    unsigned int operation = m_options.getOperationType();

    Mat structuringElement = getStructuringElement(element, Size(2 * elementSize + 1, 2 * elementSize + 1), Point(elementSize, elementSize));
    Mat result;
    if(operation <= 4) {
        // Since MORPH_X : 2,3,4,5 and 6
        morphologyEx(inputImage, result, operation + 2, structuringElement);
    } else if(operation == 5) {
        dilate(inputImage, result, structuringElement);
    } else {
        erode(inputImage, result, structuringElement);
    }
    return result;
}
MorphologyOption MorphologyCommand::options() const
{
    return m_options;
}

void MorphologyCommand::setOptions(const MorphologyOption &options)
{
    m_options = options;
}

