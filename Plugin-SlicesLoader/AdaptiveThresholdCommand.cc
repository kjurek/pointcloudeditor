#include <iostream>
#include "AdaptiveThresholdCommand.hh"

using namespace cv;

cv::Mat AdaptiveThresholdCommand::execute(cv::Mat &inputImage)
{
    Mat result;
    std::cout << "method: " << m_options.thresholdType() << " block_size: " << m_options.blockSize() << " constant: " << m_options.constant();
    adaptiveThreshold(inputImage, result, 255, m_options.thresholdType(), CV_THRESH_BINARY_INV, m_options.blockSize(), m_options.constant());
    return result;
}
AdaptiveThresholdOption AdaptiveThresholdCommand::options() const
{
    return m_options;
}

void AdaptiveThresholdCommand::setOptions(const AdaptiveThresholdOption &options)
{
    m_options = options;
}

