#include <QDebug>
#include "thresholdwidget.hh"
#include "ui_thresholdwidget.hh"
#include "ThresholdCommand.hh"

ThresholdWidget::ThresholdWidget(QWidget *parent) :
    OperationWidget(parent),
    ui(new Ui::ThresholdWidget)
{
    ui->setupUi(this);
    setOperation(std::make_shared<ThresholdCommand>());
    setupWidget();
}

ThresholdWidget::ThresholdWidget(std::shared_ptr<ImageCommand> command, QWidget *parent) :
    OperationWidget(parent),
    ui(new Ui::ThresholdWidget)
{
    ui->setupUi(this);
    setOperation(command);
    setupWidget();
}

ThresholdWidget::~ThresholdWidget()
{
    delete ui;
}

void ThresholdWidget::updateOptions()
{
    ThresholdCommand* c = dynamic_cast<ThresholdCommand*>(m_command.get());
    ThresholdOption options = c->options();
    options.setThresholdLevel(ui->thresholdLevelSB->value());
    options.setThresholdType(static_cast<ThresholdType>(ui->thresholdTypeCB->currentIndex()));
    c->setOptions(options);
}

void ThresholdWidget::setupWidget()
{
    ThresholdCommand* c = dynamic_cast<ThresholdCommand*>(m_command.get());
    ThresholdOption options = c->options();
    ui->thresholdLevelSB->setValue(options.thresholdLevel());
    ui->thresholdTypeCB->setCurrentIndex(options.thresholdType());
}
