#include "morphologywidget.hh"
#include "ui_morphologywidget.hh"
#include "MorphologyCommand.hh"

MorphologyWidget::MorphologyWidget(QWidget *parent) :
    OperationWidget(parent),
    ui(new Ui::MorphologyWidget)
{
    ui->setupUi(this);
    setOperation(std::make_shared<MorphologyCommand>());
    setupWidget();
}

MorphologyWidget::MorphologyWidget(std::shared_ptr<ImageCommand> command, QWidget *parent) :
    OperationWidget(parent),
    ui(new Ui::MorphologyWidget)
{
    ui->setupUi(this);
    setOperation(command);
    setupWidget();
}

MorphologyWidget::~MorphologyWidget()
{
    delete ui;
}

void MorphologyWidget::updateOptions()
{
    MorphologyCommand* c = dynamic_cast<MorphologyCommand*>(m_command.get());
    MorphologyOption options = c->options();
    options.setElement(static_cast<MorphologyElementType>(ui->elementCB->currentIndex()));
    options.setElementSize(ui->elementSizeSB->value());
    options.setOperationType(static_cast<MorphologyOperationType>(ui->operationCB->currentIndex()));
    c->setOptions(options);
}

void MorphologyWidget::setupWidget()
{
    MorphologyCommand* c = dynamic_cast<MorphologyCommand*>(m_command.get());
    MorphologyOption options = c->options();
    ui->elementCB->setCurrentIndex(options.getElement());
    ui->operationCB->setCurrentIndex(options.getOperationType());
    ui->elementSizeSB->setValue(options.getElementSize());
}
