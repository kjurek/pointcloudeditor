#ifndef __ContourCommand_H_
#define __ContourCommand_H_

#include "ImageCommand.hh"
#include "MorphologyOption.hh"

class MorphologyCommand : public ImageCommand
{
public:
    MorphologyCommand() { }
    MorphologyCommand(const MorphologyOption& options) : m_options(options) { }
    MorphologyCommand(std::istream& input) : m_options(input) { }

    virtual cv::Mat execute(cv::Mat& inputImage);
    virtual std::string name() { return "Morphology"; }
    virtual std::string usage()
    {
        std::stringstream ss;
        ss << name() << std::endl
           << MorphologyOption::avaiableOptions();
        return ss.str();
    }

    ImageCommandType getType() { return Morphology; }
    MorphologyOption options() const;
    void setOptions(const MorphologyOption &options);

private:
    MorphologyOption m_options;
};


#endif //__ContourCommand_H_
