#include "ThresholdCommand.hh"

using namespace cv;

cv::Mat ThresholdCommand::execute(cv::Mat &inputImage)
{
    Mat result;
    threshold( inputImage, result, m_options.thresholdLevel(), 255, m_options.thresholdType());
    return result;
}
ThresholdOption ThresholdCommand::options() const
{
    return m_options;
}

void ThresholdCommand::setOptions(const ThresholdOption &options)
{
    m_options = options;
}

