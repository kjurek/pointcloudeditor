#ifndef OPERATIONWIDGET_HH
#define OPERATIONWIDGET_HH

#include <QWidget>
#include <memory>
#include "ImageCommand.hh"

class OperationWidget : public QWidget
{
    Q_OBJECT
public:
    OperationWidget(QWidget* parent) : QWidget(parent) { }
    virtual std::shared_ptr<ImageCommand> getOperation() { updateOptions(); return m_command; }
    virtual void setOperation(std::shared_ptr<ImageCommand> command) { m_command = command; }
protected:
    std::shared_ptr<ImageCommand> m_command;
    virtual void updateOptions() = 0;
};

#endif // OPERATIONWIDGET_HH
