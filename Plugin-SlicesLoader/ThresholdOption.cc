#include <stdexcept>
#include <iostream>
#include "ThresholdOption.hh"

using namespace std;

void ThresholdOption::parseInput(std::istream& input)
{
    unsigned int thresholdLevel = 0, thresholdType = 0;
    bool isValid = !((input >> thresholdLevel >> thresholdType).rdstate() & ios::failbit);

    if(!isValid)
    {
        throw invalid_argument("TresholdOption cannot parse given input");
    }

    if(thresholdLevel > 255)
    {
        throw invalid_argument("Incorrect treshold level");
    }

    if(thresholdType < 0 || thresholdType > 4)
    {
        throw invalid_argument("Incorrect treshold type");
    }

    m_thresholdLevel = thresholdLevel;
    m_thresholdType = static_cast<ThresholdType>(thresholdType);
}

string ThresholdOption::avaiableOptions()
{
    ostringstream ss;
    ss
        << "treshold_level:" << endl
        << "\tvalue between 0 and 255, determines which pixels should be binarized (" << endl
        << "trehold_type:" << endl
        << "\t0: Binary" << endl
        << "\t1: Binary Inverted" << endl
        << "\t2: Threshold Truncated" << endl
        << "\t3: Threshold to Zero" << endl
        << "\t4: Threshold to Zero Inverted" << endl;
    return ss.str();
}

unsigned int ThresholdOption::thresholdLevel() const
{
    return m_thresholdLevel;
}

void ThresholdOption::setThresholdLevel(unsigned int thresholdLevel)
{
    m_thresholdLevel = thresholdLevel;
}
ThresholdType ThresholdOption::thresholdType() const
{
    return m_thresholdType;
}

void ThresholdOption::setThresholdType(const ThresholdType &thresholdType)
{
    m_thresholdType = thresholdType;
}


