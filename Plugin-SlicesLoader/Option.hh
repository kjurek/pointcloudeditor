#ifndef __Option_H_
#define __Option_H_

#include <sstream>

class Option {
public:
    virtual void parseInput(std::istream& input) = 0;
};


#endif //OPTION_H_
