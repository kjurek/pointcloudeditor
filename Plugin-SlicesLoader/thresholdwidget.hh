#ifndef THRESHOLDWIDGET_HH
#define THRESHOLDWIDGET_HH

#include <QWidget>
#include "operationwidget.hh"

namespace Ui {
class ThresholdWidget;
}

class ThresholdWidget : public OperationWidget
{
public:
    explicit ThresholdWidget(QWidget *parent = 0);
    explicit ThresholdWidget(std::shared_ptr<ImageCommand> command, QWidget *parent = 0);
    ~ThresholdWidget();
protected:
    virtual void updateOptions();
private:
    Ui::ThresholdWidget *ui;
    void setupWidget();
};

#endif // THRESHOLDWIDGET_HH
