#include <stdexcept>
#include <iostream>
#include "AdaptiveThresholdOption.hh"

using namespace std;

void AdaptiveThresholdOption::parseInput(std::istream& input)
{
    unsigned int thresholdType = 0;
    unsigned int blockSize = 0;
    unsigned int constant = 0;
    bool isValid = !((input >> thresholdType >> blockSize >> constant).rdstate() & ios::failbit);

    if(!isValid)
    {
        throw invalid_argument("TresholdOption cannot parse given input");
    }

    if(thresholdType > 1)
    {
        throw invalid_argument("Incorrect treshold type");
    }

    m_thresholdType = static_cast<AdaptiveThresholdType>(thresholdType);
    m_blockSize = blockSize;
    m_constant = constant;
}

string AdaptiveThresholdOption::avaiableOptions()
{
    ostringstream ss;
    ss
            << "treshold_type:" << endl
            << "\t0: Adaptive Treshold Mean" << endl
            << "\t1: Adaptive Treshold Gaussian" << endl
            << "block_size:" << endl
            << "\tIt decides the size of neighbourhood area." << endl
            << "constant:" << endl
            << "\tIt is just a constant which is subtracted from the mean or weighted mean calculated." << endl;
    return ss.str();
}
AdaptiveThresholdType AdaptiveThresholdOption::thresholdType() const
{
    return m_thresholdType;
}

void AdaptiveThresholdOption::setThresholdType(const AdaptiveThresholdType &thresholdType)
{
    m_thresholdType = thresholdType;
}
unsigned int AdaptiveThresholdOption::blockSize() const
{
    return m_blockSize;
}

void AdaptiveThresholdOption::setBlockSize(unsigned int blockSize)
{
    m_blockSize = blockSize;
}

unsigned int AdaptiveThresholdOption::constant() const
{
    return m_constant;
}

void AdaptiveThresholdOption::setConstant(unsigned int constant)
{
    m_constant = constant;
}



