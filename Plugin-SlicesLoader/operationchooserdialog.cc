#include "operationchooserdialog.hh"
#include "ui_operationchooserdialog.hh"
#include "ImageCommand.hh"
#include "morphologywidget.hh"
#include "thresholdwidget.hh"
#include "adaptivethresholdwidget.hh"
#include <QDebug>

OperationChooserDialog::OperationChooserDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OperationChooserDialog),
    m_operationWidget(new MorphologyWidget)
{
    ui->setupUi(this);
    handleOperationSelected(Morphology);
    connect(ui->operationComboBox, SIGNAL(currentIndexChanged(int)),this,SLOT(handleOperationSelected(int)));
}

OperationChooserDialog::OperationChooserDialog(std::shared_ptr<ImageCommand> command, QWidget *parent)
    : QDialog(parent), ui(new Ui::OperationChooserDialog)
{
    ui->setupUi(this);
    switch (command->getType()) {
    case Morphology:
        m_operationWidget = std::make_shared<MorphologyWidget>(command);
        break;
    case Threshold:
        m_operationWidget = std::make_shared<ThresholdWidget>(command);
        break;
    case AdaptiveThreshold:
        m_operationWidget = std::make_shared<AdaptiveThresholdWidget>(command);
    default:
        break;
    }
    ui->operationComboBox->setCurrentIndex(m_operationWidget->getOperation()->getType());
    ui->verticalLayout_2->addWidget(m_operationWidget.get());
    connect(ui->operationComboBox, SIGNAL(currentIndexChanged(int)),this,SLOT(handleOperationSelected(int)));
}

OperationChooserDialog::~OperationChooserDialog()
{
    delete ui;
}

void OperationChooserDialog::handleOperationSelected(int index)
{
    if(m_operationWidget != nullptr)
    {
        ui->verticalLayout_2->removeWidget(m_operationWidget.get());
    }

    switch (index) {
    case Threshold:
        m_operationWidget = std::make_shared<ThresholdWidget>();
        break;
    case AdaptiveThreshold:
        m_operationWidget = std::make_shared<AdaptiveThresholdWidget>();
        break;
    case Morphology:
        m_operationWidget = std::make_shared<MorphologyWidget>();
        break;
    default:
        break;
    }
    ui->verticalLayout_2->addWidget(m_operationWidget.get());
}

std::shared_ptr<OperationWidget> OperationChooserDialog::getOperationWidget() const
{
    return m_operationWidget;
}

void OperationChooserDialog::setOperationWidget(std::shared_ptr<OperationWidget> value)
{
    m_operationWidget = value;
}

void OperationChooserDialog::reject()
{
    m_operationWidget = nullptr;
    QDialog::reject();
}

