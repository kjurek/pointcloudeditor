#ifndef __TresholdOption_H
#define __TresholdOption_H

#include "Option.hh"

enum ThresholdType
{
    Binary, BinaryInverted, Truncated, ToZero, ToZeroInverted
};

class ThresholdOption : public Option
{
public:
    ThresholdOption() : m_thresholdLevel(0), m_thresholdType(Binary) { }
    ThresholdOption(std::istream& input) { parseInput(input); }

    virtual void parseInput(std::istream& input);
    static std::string avaiableOptions();

    unsigned int thresholdLevel() const;
    void setThresholdLevel(unsigned int thresholdLevel);

    ThresholdType thresholdType() const;
    void setThresholdType(const ThresholdType &thresholdType);

private:
    unsigned int m_thresholdLevel;
    ThresholdType m_thresholdType;
};

#endif
