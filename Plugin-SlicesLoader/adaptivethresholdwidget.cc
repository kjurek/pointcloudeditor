#include "adaptivethresholdwidget.hh"
#include "ui_adaptivethresholdwidget.hh"
#include "AdaptiveThresholdCommand.hh"

AdaptiveThresholdWidget::AdaptiveThresholdWidget(QWidget *parent) :
    OperationWidget(parent),
    ui(new Ui::AdaptiveThresholdWidget)
{
    ui->setupUi(this);
    setOperation(std::make_shared<AdaptiveThresholdCommand>());
    setupWidget();
}

AdaptiveThresholdWidget::AdaptiveThresholdWidget(std::shared_ptr<ImageCommand> command, QWidget *parent) :
    OperationWidget(parent),
    ui(new Ui::AdaptiveThresholdWidget)
{
    ui->setupUi(this);
    setOperation(command);
    setupWidget();
}

AdaptiveThresholdWidget::~AdaptiveThresholdWidget()
{
    delete ui;
}

void AdaptiveThresholdWidget::setupWidget()
{
    AdaptiveThresholdCommand* c = dynamic_cast<AdaptiveThresholdCommand*>(m_command.get());
    AdaptiveThresholdOption options = c->options();
    ui->adaptiveThresholdTypeCB->setCurrentIndex(options.thresholdType());
    ui->blockSizeSB->setValue(options.blockSize());
    ui->constantSB->setValue(options.constant());
}

void AdaptiveThresholdWidget::updateOptions()
{
    AdaptiveThresholdCommand* c = dynamic_cast<AdaptiveThresholdCommand*>(m_command.get());
    AdaptiveThresholdOption options = c->options();
    options.setThresholdType(static_cast<AdaptiveThresholdType>(ui->adaptiveThresholdTypeCB->currentIndex()));
    options.setBlockSize(ui->blockSizeSB->value());
    options.setConstant(ui->constantSB->value());
    c->setOptions(options);
}
