#ifndef __AdaptiveTresholdCommand_H_
#define __AdaptiveTresholdCommand_H_

#include "AdaptiveThresholdOption.hh"
#include "ImageCommand.hh"

class AdaptiveThresholdCommand : public ImageCommand {
public:
    AdaptiveThresholdCommand() { }
    AdaptiveThresholdCommand(std::istream& input) : m_options(input) { }
    virtual cv::Mat execute(cv::Mat& inputImage);

    virtual std::string name() { return "Adaptive Threshold"; }
    virtual std::string usage()
    {
        return name() + "\n" + AdaptiveThresholdOption::avaiableOptions();
    }
    ImageCommandType getType() { return AdaptiveThreshold; }

    AdaptiveThresholdOption options() const;
    void setOptions(const AdaptiveThresholdOption &options);

private:
    AdaptiveThresholdOption m_options;
};


#endif //__AdaptiveTresholdCommand_H_
