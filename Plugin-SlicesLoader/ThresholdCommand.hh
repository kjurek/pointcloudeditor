#ifndef __TresholdCommand_H_
#define __TresholdCommand_H_

#include <sstream>
#include "ImageCommand.hh"
#include "ThresholdOption.hh"

class ThresholdCommand : public ImageCommand {
public:
    ThresholdCommand() { }
    ThresholdCommand(std::istream& input) : m_options(input) { }
    virtual cv::Mat execute(cv::Mat& inputImage);

    virtual std::string name() { return "Threshold"; }
    virtual std::string usage()
    {
        return name() + "\n" + ThresholdOption::avaiableOptions();
    }
    ImageCommandType getType() { return Threshold; }

    ThresholdOption options() const;
    void setOptions(const ThresholdOption &options);

private:
    ThresholdOption m_options;
};


#endif //__TresholdCommand_H_
