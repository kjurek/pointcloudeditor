#ifndef SLICESLOADERPLUGIN_H
#define SLICESLOADERPLUGIN_H

#include <QObject>
#include <OpenFlipper/common/Types.hh>
#include <OpenFlipper/BasePlugin/BaseInterface.hh>
#include <OpenFlipper/BasePlugin/MenuInterface.hh>


class SlicesLoaderPlugin : public QObject, BaseInterface, MenuInterface
{
    Q_OBJECT
    Q_INTERFACES(BaseInterface)
    Q_INTERFACES(MenuInterface)

    #if QT_VERSION >= 0x050000
      Q_PLUGIN_METADATA(IID "org.OpenFlipper.Plugins.Plugin-SlicesLoader")
    #endif

signals:
    void addMenubarAction(QAction* _action, QString _type);
    void getMenubarMenu();

private slots:
    void pluginsInitialized();

public:
    QString name() { return QString("SlicesLoader Plugin"); }
    QString description() { return QString(tr("Provides loading model from slices")); }

public slots:
    QString version() { return QString("1.0"); }
    void loadSlices();
};

#endif
