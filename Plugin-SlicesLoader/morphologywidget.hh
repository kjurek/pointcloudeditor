#ifndef MORPHOLOGYCHOOSERWIDGET_HH
#define MORPHOLOGYCHOOSERWIDGET_HH

#include "operationwidget.hh"


namespace Ui {
class MorphologyWidget;
}

class MorphologyWidget : public OperationWidget
{
public:
    explicit MorphologyWidget(QWidget *parent = 0);
    explicit MorphologyWidget(std::shared_ptr<ImageCommand> command, QWidget *parent = 0);
    ~MorphologyWidget();
protected:
    virtual void updateOptions();
private:
    Ui::MorphologyWidget *ui;
    void setupWidget();
};

#endif // MORPHOLOGYCHOOSERWIDGET_HH
