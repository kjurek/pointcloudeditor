#ifndef __ImageCommand_H_
#define __ImageCommand_H_

#include <string>
#include "Option.hh"
#include "opencv2/imgproc/imgproc.hpp"

enum ImageCommandType
{
    Morphology, Threshold, AdaptiveThreshold
};

class ImageCommand
{
public:
    virtual cv::Mat execute(cv::Mat& inputImage) = 0;
    virtual ImageCommandType getType() = 0;
    virtual std::string name() = 0;
    virtual std::string usage() = 0;
};


#endif //__ImageCommand_H_
