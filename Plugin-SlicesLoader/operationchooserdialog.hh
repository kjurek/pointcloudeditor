#ifndef OPERATIONCHOOSERDIALOG_HH
#define OPERATIONCHOOSERDIALOG_HH

#include <QDialog>
#include "operationwidget.hh"

namespace Ui {
class OperationChooserDialog;
}

class OperationChooserDialog : public QDialog
{
    Q_OBJECT

public:
    explicit OperationChooserDialog(QWidget *parent = 0);
    explicit OperationChooserDialog(std::shared_ptr<ImageCommand> command, QWidget* parent = 0);
    ~OperationChooserDialog();

    std::shared_ptr<OperationWidget> getOperationWidget() const;
    void setOperationWidget(std::shared_ptr<OperationWidget> value);

private slots:
    void reject();
    void handleOperationSelected(int index);

private:
    Ui::OperationChooserDialog *ui;
    std::shared_ptr<OperationWidget> m_operationWidget;
};

#endif // OPERATIONCHOOSERDIALOG_HH
