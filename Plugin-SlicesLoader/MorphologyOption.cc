#include <string>
#include <stdexcept>
#include "MorphologyOption.hh"

using namespace std;

void MorphologyOption::parseInput(std::istream& input)
{
    unsigned int operationType = 0, element = 0, elementSize = 0;
    bool isValid = !((input >> operationType >> element >> elementSize).rdstate() & ios::failbit);

    if(!isValid)
    {
        throw invalid_argument("MorphologyOption cannot parse given input");
    }

    if(operationType > 6)
    {
        throw invalid_argument("Incorrect operation type");
    }

    if(element > 2)
    {
        throw invalid_argument("Invalid element type");
    }

    if(elementSize < 1)
    {
        throw invalid_argument("Incorrect element size");
    }

    m_operationType = static_cast<MorphologyOperationType>(operationType);
    m_element = static_cast<MorphologyElementType>(element);
    m_elementSize = elementSize;
}

string MorphologyOption::avaiableOptions()
{
    ostringstream ss;
    ss
            << "operation_type:" << endl
            << "\t0: Opening" << endl
            << "\t1: Closing" << endl
            << "\t2: Gradient" << endl
            << "\t3: Top Hat" << endl
            << "\t4: Black Hat" << endl
            << "\t5: Dilatation" << endl
            << "\t6: Erosion" << endl
            << "element: type of structuring element" << endl
            << "\t0: Rect" << endl
            << "\t1: Cross" << endl
            << "\t2: Ellipse" << endl
            << "element_size: value bigger then 0 (structuring element size)" << endl;
    return ss.str();
}

