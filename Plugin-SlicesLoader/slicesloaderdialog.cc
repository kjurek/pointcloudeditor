#include "slicesloaderdialog.hh"
#include "ui_slicesloaderdialog.hh"
#include "operationchooserdialog.hh"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <QDir>
#include <QMessageBox>
#include <QDirModel>
#include <QDebug>
#include <QFileDialog>
#include <QShortcut>
using namespace cv;

SlicesLoaderDialog::SlicesLoaderDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SlicesLoaderDialog)
{
    ui->setupUi(this);
    model = new QFileSystemModel();
    model->setRootPath(QDir::homePath());
    m_filters << "*.jpg" << "*.png" << "*.bmp";
    model->setNameFilters(m_filters);
    model->setNameFilterDisables(false);
    ui->treeView->setAnimated(false);
    ui->treeView->setIndentation(20);
    ui->treeView->setSortingEnabled(true);
    ui->treeView->setModel(model);
    ui->treeView->setRootIndex(model->index(QDir::homePath()));
    ui->treeView->setSelectionMode(QTreeView::ExtendedSelection);
    ui->treeView->setSelectionBehavior(QTreeView::SelectRows);

    connect(ui->browseButton, SIGNAL(released()), this, SLOT(handleBrowse()));
    connect(ui->addFilterButton, SIGNAL(released()), this, SLOT(handleAddFilter()));
    connect(ui->treeView,SIGNAL(doubleClicked(QModelIndex)),this,SLOT(handleFileDoubleClick(QModelIndex)));
    connect(ui->filterList, SIGNAL(doubleClicked(QModelIndex)), this,SLOT(handleOperationDoubleClick(QModelIndex)));
    QShortcut* shortcut = new QShortcut(QKeySequence(Qt::Key_Delete), ui->filterList);
    connect(shortcut, SIGNAL(activated()), this, SLOT(handleDeleteFilter()));
}

SlicesLoaderDialog::~SlicesLoaderDialog()
{
    delete ui;
    delete model;
}

void SlicesLoaderDialog::handleBrowse()
{
    QString dirPath = QFileDialog::getExistingDirectory();
    model->setRootPath(dirPath);
    ui->treeView->setRootIndex(model->index(dirPath));
}

void SlicesLoaderDialog::handleAddFilter()
{
    OperationChooserDialog d;
    d.exec();
    std::shared_ptr<OperationWidget> operationWidget = d.getOperationWidget();
    if(operationWidget == nullptr)
    {
        return;
    }

    m_operations.push_back(operationWidget->getOperation());
    ui->filterList->addItem(operationWidget->getOperation()->name().c_str());
}

void SlicesLoaderDialog::handleFileDoubleClick(QModelIndex index)
{
    QFileInfo fileInfo = model->fileInfo(index);
    if(fileInfo.isDir())
    {
        return;
    }

    QImage inputImage(fileInfo.absoluteFilePath());
    ui->inputImage->setPixmap(QPixmap::fromImage(inputImage));

    cv::Mat outputMat = processImage(fileInfo);

    QImage outputImage(outputMat.data, outputMat.cols, outputMat.rows, outputMat.step, QImage::Format_Indexed8);
    ui->outputImage->setPixmap(QPixmap::fromImage(outputImage));
}

void SlicesLoaderDialog::handleOperationDoubleClick(QModelIndex index)
{
    OperationChooserDialog d(m_operations[index.row()]);
    d.exec();

    std::shared_ptr<OperationWidget> operationWidget = d.getOperationWidget();
    if(operationWidget == nullptr)
    {
        return;
    }

    m_operations[index.row()] = operationWidget->getOperation();
    QListWidgetItem* item = ui->filterList->item(index.row());
    item->setText(operationWidget->getOperation()->name().c_str());
}

void SlicesLoaderDialog::handleDeleteFilter()
{
    if(!m_operations.empty())
    {
        m_operations.erase(m_operations.begin() + ui->filterList->currentRow());
        delete ui->filterList->currentItem();
    }
}

void SlicesLoaderDialog::accept()
{
    QFileInfoList files = getSelectedFiles();
    for(QFileInfo file : files)
    {
        m_originalImages.push_back(imread(file.absoluteFilePath().toStdString(), CV_LOAD_IMAGE_COLOR));
        m_filteredImages.push_back(processImage(file.absoluteFilePath()));
    }
    QDialog::accept();
}
std::vector<cv::Mat> SlicesLoaderDialog::originalImages() const
{
    return m_originalImages;
}

std::vector<cv::Mat> SlicesLoaderDialog::filteredImages() const
{
    return m_filteredImages;
}

bool SlicesLoaderDialog::isImage(const QFileInfo &info)
{
    return m_filters.contains(QString("*.") + info.suffix());
}

QFileInfoList SlicesLoaderDialog::getSelectedFiles()
{
    QModelIndex index = ui->treeView->currentIndex();
    QFileInfo fileInfo = model->fileInfo(index);

    if(fileInfo.isDir())
    {
        QDir dir(fileInfo.absoluteFilePath());
        return dir.entryInfoList(m_filters, QDir::Files);
    }

    QModelIndexList list = ui->treeView->selectionModel()->selectedIndexes();
    QFileInfoList result;

    int row = -1;
    foreach (QModelIndex index, list)
    {
        if (index.row() !=row && index.column()==0)
        {
            QFileInfo fileInfo = model->fileInfo(index);
            if(isImage(fileInfo))
            {
                result << fileInfo;
            }
            row = index.row();
        }
    }
    return result;
}

Mat SlicesLoaderDialog::processImage(QFileInfo file)
{
    Mat outputMat = imread(file.absoluteFilePath().toStdString(), CV_LOAD_IMAGE_GRAYSCALE);
    for(std::shared_ptr<ImageCommand> operation : m_operations)
    {
        outputMat = operation->execute(outputMat);
    }
    return outputMat;
}
