#ifndef ADAPTIVETHRESHOLDWIDGET_HH
#define ADAPTIVETHRESHOLDWIDGET_HH

#include "operationwidget.hh"

namespace Ui {
class AdaptiveThresholdWidget;
}

class AdaptiveThresholdWidget : public OperationWidget
{
public:
    explicit AdaptiveThresholdWidget(QWidget *parent = 0);
    explicit AdaptiveThresholdWidget(std::shared_ptr<ImageCommand> command, QWidget *parent = 0);
    ~AdaptiveThresholdWidget();
private:
    Ui::AdaptiveThresholdWidget *ui;
    void setupWidget();
protected:
    virtual void updateOptions();
};

#endif // ADAPTIVETHRESHOLDWIDGET_HH
